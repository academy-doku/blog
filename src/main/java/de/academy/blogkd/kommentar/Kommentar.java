package de.academy.blogkd.kommentar;

import de.academy.blogkd.beitrag.Beitrag;
import de.academy.blogkd.user.User;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;

@Entity
public class Kommentar {

    @Id
    @GeneratedValue
    private long id;

    private String text;
    private Instant postedAt;


    @ManyToOne
    private Beitrag beitrag;
    @ManyToOne
    private User user;

    public Kommentar(String text, Instant postedAt, Beitrag beitrag, User user) {
        this.text = text;
        this.postedAt = postedAt;
        this.beitrag = beitrag;
        this.user = user;

    }

    public Kommentar(String text, Instant postedAt) {
        this.text = text;
        this.postedAt = postedAt;

    }

    public Kommentar() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public Beitrag getBeitrag() {
        return beitrag;
    }

    public void setBeitrag(Beitrag beitrag) {
        this.beitrag = beitrag;
    }

    public User getUser() {
        return user;
    }
}
