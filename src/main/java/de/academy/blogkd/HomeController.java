package de.academy.blogkd;

import de.academy.blogkd.beitrag.BeitragRepository;
import de.academy.blogkd.kommentar.KommentarRepository;
import de.academy.blogkd.user.User;
import de.academy.blogkd.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class HomeController {

    private BeitragRepository beitragRepository;
    private KommentarRepository kommentarRepository;
    private UserRepository userRepository;

    @Autowired
    public HomeController(BeitragRepository beitragRepository, KommentarRepository kommentarRepository, UserRepository userRepository) {
        this.beitragRepository = beitragRepository;
        this.kommentarRepository = kommentarRepository;
        this.userRepository = userRepository;
    }

    @GetMapping("/")
    public String home(@ModelAttribute("sessionUser") User sessionUser, @ModelAttribute("user") User user, Model model) {
        model.addAttribute("beitraege", beitragRepository.findAllByOrderByPostedAtDesc());
        model.addAttribute("kommentare", kommentarRepository.findAllByOrderByPostedAtAsc());
        return "home";
    }

}
